/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.healthipass.svcs.hip.HipClientLocationIntegrationApptTypeCharges;
import com.healthipass.svcs.hip.service.HipClientLocationIntegrationApptTypeChargesService;


/**
 * Controller object for domain model class HipClientLocationIntegrationApptTypeCharges.
 * @see HipClientLocationIntegrationApptTypeCharges
 */
@RestController("HIP.HipClientLocationIntegrationApptTypeChargesController")
@Api(value = "HipClientLocationIntegrationApptTypeChargesController", description = "Exposes APIs to work with HipClientLocationIntegrationApptTypeCharges resource.")
@RequestMapping("/HIP/HipClientLocationIntegrationApptTypeCharges")
public class HipClientLocationIntegrationApptTypeChargesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipClientLocationIntegrationApptTypeChargesController.class);

    @Autowired
	@Qualifier("HIP.HipClientLocationIntegrationApptTypeChargesService")
	private HipClientLocationIntegrationApptTypeChargesService hipClientLocationIntegrationApptTypeChargesService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new HipClientLocationIntegrationApptTypeCharges instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipClientLocationIntegrationApptTypeCharges createHipClientLocationIntegrationApptTypeCharges(@RequestBody HipClientLocationIntegrationApptTypeCharges hipClientLocationIntegrationApptTypeCharges) {
		LOGGER.debug("Create HipClientLocationIntegrationApptTypeCharges with information: {}" , hipClientLocationIntegrationApptTypeCharges);

		hipClientLocationIntegrationApptTypeCharges = hipClientLocationIntegrationApptTypeChargesService.create(hipClientLocationIntegrationApptTypeCharges);
		LOGGER.debug("Created HipClientLocationIntegrationApptTypeCharges with information: {}" , hipClientLocationIntegrationApptTypeCharges);

	    return hipClientLocationIntegrationApptTypeCharges;
	}

    @ApiOperation(value = "Returns the HipClientLocationIntegrationApptTypeCharges instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipClientLocationIntegrationApptTypeCharges getHipClientLocationIntegrationApptTypeCharges(@PathVariable("id") Integer id) {
        LOGGER.debug("Getting HipClientLocationIntegrationApptTypeCharges with id: {}" , id);

        HipClientLocationIntegrationApptTypeCharges foundHipClientLocationIntegrationApptTypeCharges = hipClientLocationIntegrationApptTypeChargesService.getById(id);
        LOGGER.debug("HipClientLocationIntegrationApptTypeCharges details with id: {}" , foundHipClientLocationIntegrationApptTypeCharges);

        return foundHipClientLocationIntegrationApptTypeCharges;
    }

    @ApiOperation(value = "Updates the HipClientLocationIntegrationApptTypeCharges instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipClientLocationIntegrationApptTypeCharges editHipClientLocationIntegrationApptTypeCharges(@PathVariable("id") Integer id, @RequestBody HipClientLocationIntegrationApptTypeCharges hipClientLocationIntegrationApptTypeCharges) {
        LOGGER.debug("Editing HipClientLocationIntegrationApptTypeCharges with id: {}" , hipClientLocationIntegrationApptTypeCharges.getId());

        hipClientLocationIntegrationApptTypeCharges.setId(id);
        hipClientLocationIntegrationApptTypeCharges = hipClientLocationIntegrationApptTypeChargesService.update(hipClientLocationIntegrationApptTypeCharges);
        LOGGER.debug("HipClientLocationIntegrationApptTypeCharges details with id: {}" , hipClientLocationIntegrationApptTypeCharges);

        return hipClientLocationIntegrationApptTypeCharges;
    }

    @ApiOperation(value = "Deletes the HipClientLocationIntegrationApptTypeCharges instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteHipClientLocationIntegrationApptTypeCharges(@PathVariable("id") Integer id) {
        LOGGER.debug("Deleting HipClientLocationIntegrationApptTypeCharges with id: {}" , id);

        HipClientLocationIntegrationApptTypeCharges deletedHipClientLocationIntegrationApptTypeCharges = hipClientLocationIntegrationApptTypeChargesService.delete(id);

        return deletedHipClientLocationIntegrationApptTypeCharges != null;
    }

    /**
     * @deprecated Use {@link #findHipClientLocationIntegrationApptTypeCharges(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of HipClientLocationIntegrationApptTypeCharges instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipClientLocationIntegrationApptTypeCharges> searchHipClientLocationIntegrationApptTypeChargesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering HipClientLocationIntegrationApptTypeCharges list by query filter:{}", (Object) queryFilters);
        return hipClientLocationIntegrationApptTypeChargesService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of HipClientLocationIntegrationApptTypeCharges instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipClientLocationIntegrationApptTypeCharges> findHipClientLocationIntegrationApptTypeCharges(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering HipClientLocationIntegrationApptTypeCharges list by filter:", query);
        return hipClientLocationIntegrationApptTypeChargesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of HipClientLocationIntegrationApptTypeCharges instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipClientLocationIntegrationApptTypeCharges> filterHipClientLocationIntegrationApptTypeCharges(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering HipClientLocationIntegrationApptTypeCharges list by filter", query);
        return hipClientLocationIntegrationApptTypeChargesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportHipClientLocationIntegrationApptTypeCharges(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return hipClientLocationIntegrationApptTypeChargesService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StringWrapper exportHipClientLocationIntegrationApptTypeChargesAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = HipClientLocationIntegrationApptTypeCharges.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> hipClientLocationIntegrationApptTypeChargesService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of HipClientLocationIntegrationApptTypeCharges instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countHipClientLocationIntegrationApptTypeCharges( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting HipClientLocationIntegrationApptTypeCharges");
		return hipClientLocationIntegrationApptTypeChargesService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getHipClientLocationIntegrationApptTypeChargesAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return hipClientLocationIntegrationApptTypeChargesService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service HipClientLocationIntegrationApptTypeChargesService instance
	 */
	protected void setHipClientLocationIntegrationApptTypeChargesService(HipClientLocationIntegrationApptTypeChargesService service) {
		this.hipClientLocationIntegrationApptTypeChargesService = service;
	}

}