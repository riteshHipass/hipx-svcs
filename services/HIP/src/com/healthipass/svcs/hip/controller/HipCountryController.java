/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.healthipass.svcs.hip.HipCountry;
import com.healthipass.svcs.hip.HipState;
import com.healthipass.svcs.hip.service.HipCountryService;


/**
 * Controller object for domain model class HipCountry.
 * @see HipCountry
 */
@RestController("HIP.HipCountryController")
@Api(value = "HipCountryController", description = "Exposes APIs to work with HipCountry resource.")
@RequestMapping("/HIP/HipCountry")
public class HipCountryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipCountryController.class);

    @Autowired
	@Qualifier("HIP.HipCountryService")
	private HipCountryService hipCountryService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new HipCountry instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipCountry createHipCountry(@RequestBody HipCountry hipCountry) {
		LOGGER.debug("Create HipCountry with information: {}" , hipCountry);

		hipCountry = hipCountryService.create(hipCountry);
		LOGGER.debug("Created HipCountry with information: {}" , hipCountry);

	    return hipCountry;
	}

    @ApiOperation(value = "Returns the HipCountry instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipCountry getHipCountry(@PathVariable("id") Integer id) {
        LOGGER.debug("Getting HipCountry with id: {}" , id);

        HipCountry foundHipCountry = hipCountryService.getById(id);
        LOGGER.debug("HipCountry details with id: {}" , foundHipCountry);

        return foundHipCountry;
    }

    @ApiOperation(value = "Updates the HipCountry instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public HipCountry editHipCountry(@PathVariable("id") Integer id, @RequestBody HipCountry hipCountry) {
        LOGGER.debug("Editing HipCountry with id: {}" , hipCountry.getId());

        hipCountry.setId(id);
        hipCountry = hipCountryService.update(hipCountry);
        LOGGER.debug("HipCountry details with id: {}" , hipCountry);

        return hipCountry;
    }

    @ApiOperation(value = "Deletes the HipCountry instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteHipCountry(@PathVariable("id") Integer id) {
        LOGGER.debug("Deleting HipCountry with id: {}" , id);

        HipCountry deletedHipCountry = hipCountryService.delete(id);

        return deletedHipCountry != null;
    }

    /**
     * @deprecated Use {@link #findHipCountries(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of HipCountry instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipCountry> searchHipCountriesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering HipCountries list by query filter:{}", (Object) queryFilters);
        return hipCountryService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of HipCountry instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipCountry> findHipCountries(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering HipCountries list by filter:", query);
        return hipCountryService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of HipCountry instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipCountry> filterHipCountries(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering HipCountries list by filter", query);
        return hipCountryService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportHipCountries(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return hipCountryService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public StringWrapper exportHipCountriesAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = HipCountry.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> hipCountryService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of HipCountry instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countHipCountries( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting HipCountries");
		return hipCountryService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getHipCountryAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return hipCountryService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/hipStates", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the hipStates instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<HipState> findAssociatedHipStates(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated hipStates");
        return hipCountryService.findAssociatedHipStates(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service HipCountryService instance
	 */
	protected void setHipCountryService(HipCountryService service) {
		this.hipCountryService = service;
	}

}