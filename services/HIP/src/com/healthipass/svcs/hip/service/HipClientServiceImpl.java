/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.healthipass.svcs.hip.HipClient;
import com.healthipass.svcs.hip.HipClientLocation;


/**
 * ServiceImpl object for domain model class HipClient.
 *
 * @see HipClient
 */
@Service("HIP.HipClientService")
@Validated
public class HipClientServiceImpl implements HipClientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipClientServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("HIP.HipClientLocationService")
    private HipClientLocationService hipClientLocationService;

    @Autowired
    @Qualifier("HIP.HipClientDao")
    private WMGenericDao<HipClient, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<HipClient, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClient create(HipClient hipClient) {
        LOGGER.debug("Creating a new HipClient with information: {}", hipClient);

        HipClient hipClientCreated = this.wmGenericDao.create(hipClient);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(hipClientCreated);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClient getById(Integer hipclientId) {
        LOGGER.debug("Finding HipClient by id: {}", hipclientId);
        return this.wmGenericDao.findById(hipclientId);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClient findById(Integer hipclientId) {
        LOGGER.debug("Finding HipClient by id: {}", hipclientId);
        try {
            return this.wmGenericDao.findById(hipclientId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No HipClient found with id: {}", hipclientId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public List<HipClient> findByMultipleIds(List<Integer> hipclientIds, boolean orderedReturn) {
        LOGGER.debug("Finding HipClients by ids: {}", hipclientIds);

        return this.wmGenericDao.findByMultipleIds(hipclientIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "HIPTransactionManager")
    @Override
    public HipClient update(HipClient hipClient) {
        LOGGER.debug("Updating HipClient with information: {}", hipClient);

        this.wmGenericDao.update(hipClient);
        this.wmGenericDao.refresh(hipClient);

        return hipClient;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClient delete(Integer hipclientId) {
        LOGGER.debug("Deleting HipClient with id: {}", hipclientId);
        HipClient deleted = this.wmGenericDao.findById(hipclientId);
        if (deleted == null) {
            LOGGER.debug("No HipClient found with id: {}", hipclientId);
            throw new EntityNotFoundException(String.valueOf(hipclientId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public void delete(HipClient hipClient) {
        LOGGER.debug("Deleting HipClient with {}", hipClient);
        this.wmGenericDao.delete(hipClient);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClient> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all HipClients");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClient> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all HipClients");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service HIP for table HipClient to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service HIP for table HipClient to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocation> findAssociatedHipClientLocations(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated hipClientLocations");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("hipClient.id = '" + id + "'");

        return hipClientLocationService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service HipClientLocationService instance
     */
    protected void setHipClientLocationService(HipClientLocationService service) {
        this.hipClientLocationService = service;
    }

}