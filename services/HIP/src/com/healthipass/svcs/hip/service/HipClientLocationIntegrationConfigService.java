/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.healthipass.svcs.hip.HipClientLocationIntegrationConfig;

/**
 * Service object for domain model class {@link HipClientLocationIntegrationConfig}.
 */
public interface HipClientLocationIntegrationConfigService {

    /**
     * Creates a new HipClientLocationIntegrationConfig. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on HipClientLocationIntegrationConfig if any.
     *
     * @param hipClientLocationIntegrationConfig Details of the HipClientLocationIntegrationConfig to be created; value cannot be null.
     * @return The newly created HipClientLocationIntegrationConfig.
     */
    HipClientLocationIntegrationConfig create(@Valid HipClientLocationIntegrationConfig hipClientLocationIntegrationConfig);


	/**
     * Returns HipClientLocationIntegrationConfig by given id if exists.
     *
     * @param hipclientlocationintegrationconfigId The id of the HipClientLocationIntegrationConfig to get; value cannot be null.
     * @return HipClientLocationIntegrationConfig associated with the given hipclientlocationintegrationconfigId.
	 * @throws EntityNotFoundException If no HipClientLocationIntegrationConfig is found.
     */
    HipClientLocationIntegrationConfig getById(Integer hipclientlocationintegrationconfigId);

    /**
     * Find and return the HipClientLocationIntegrationConfig by given id if exists, returns null otherwise.
     *
     * @param hipclientlocationintegrationconfigId The id of the HipClientLocationIntegrationConfig to get; value cannot be null.
     * @return HipClientLocationIntegrationConfig associated with the given hipclientlocationintegrationconfigId.
     */
    HipClientLocationIntegrationConfig findById(Integer hipclientlocationintegrationconfigId);

	/**
     * Find and return the list of HipClientLocationIntegrationConfigs by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param hipclientlocationintegrationconfigIds The id's of the HipClientLocationIntegrationConfig to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return HipClientLocationIntegrationConfigs associated with the given hipclientlocationintegrationconfigIds.
     */
    List<HipClientLocationIntegrationConfig> findByMultipleIds(List<Integer> hipclientlocationintegrationconfigIds, boolean orderedReturn);


    /**
     * Updates the details of an existing HipClientLocationIntegrationConfig. It replaces all fields of the existing HipClientLocationIntegrationConfig with the given hipClientLocationIntegrationConfig.
     *
     * This method overrides the input field values using Server side or database managed properties defined on HipClientLocationIntegrationConfig if any.
     *
     * @param hipClientLocationIntegrationConfig The details of the HipClientLocationIntegrationConfig to be updated; value cannot be null.
     * @return The updated HipClientLocationIntegrationConfig.
     * @throws EntityNotFoundException if no HipClientLocationIntegrationConfig is found with given input.
     */
    HipClientLocationIntegrationConfig update(@Valid HipClientLocationIntegrationConfig hipClientLocationIntegrationConfig);

    /**
     * Deletes an existing HipClientLocationIntegrationConfig with the given id.
     *
     * @param hipclientlocationintegrationconfigId The id of the HipClientLocationIntegrationConfig to be deleted; value cannot be null.
     * @return The deleted HipClientLocationIntegrationConfig.
     * @throws EntityNotFoundException if no HipClientLocationIntegrationConfig found with the given id.
     */
    HipClientLocationIntegrationConfig delete(Integer hipclientlocationintegrationconfigId);

    /**
     * Deletes an existing HipClientLocationIntegrationConfig with the given object.
     *
     * @param hipClientLocationIntegrationConfig The instance of the HipClientLocationIntegrationConfig to be deleted; value cannot be null.
     */
    void delete(HipClientLocationIntegrationConfig hipClientLocationIntegrationConfig);

    /**
     * Find all HipClientLocationIntegrationConfigs matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching HipClientLocationIntegrationConfigs.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<HipClientLocationIntegrationConfig> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all HipClientLocationIntegrationConfigs matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching HipClientLocationIntegrationConfigs.
     *
     * @see Pageable
     * @see Page
     */
    Page<HipClientLocationIntegrationConfig> findAll(String query, Pageable pageable);

    /**
     * Exports all HipClientLocationIntegrationConfigs matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all HipClientLocationIntegrationConfigs matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the HipClientLocationIntegrationConfigs in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the HipClientLocationIntegrationConfig.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}