/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.healthipass.svcs.hip.HipClientLocationIntegrationApptTypeCharges;
import com.healthipass.svcs.hip.HipClientLocationIntegrationCodeset;


/**
 * ServiceImpl object for domain model class HipClientLocationIntegrationCodeset.
 *
 * @see HipClientLocationIntegrationCodeset
 */
@Service("HIP.HipClientLocationIntegrationCodesetService")
@Validated
public class HipClientLocationIntegrationCodesetServiceImpl implements HipClientLocationIntegrationCodesetService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipClientLocationIntegrationCodesetServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("HIP.HipClientLocationIntegrationApptTypeChargesService")
    private HipClientLocationIntegrationApptTypeChargesService hipClientLocationIntegrationApptTypeChargesService;

    @Autowired
    @Qualifier("HIP.HipClientLocationIntegrationCodesetDao")
    private WMGenericDao<HipClientLocationIntegrationCodeset, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<HipClientLocationIntegrationCodeset, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationCodeset create(HipClientLocationIntegrationCodeset hipClientLocationIntegrationCodeset) {
        LOGGER.debug("Creating a new HipClientLocationIntegrationCodeset with information: {}", hipClientLocationIntegrationCodeset);

        HipClientLocationIntegrationCodeset hipClientLocationIntegrationCodesetCreated = this.wmGenericDao.create(hipClientLocationIntegrationCodeset);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(hipClientLocationIntegrationCodesetCreated);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationCodeset getById(Integer hipclientlocationintegrationcodesetId) {
        LOGGER.debug("Finding HipClientLocationIntegrationCodeset by id: {}", hipclientlocationintegrationcodesetId);
        return this.wmGenericDao.findById(hipclientlocationintegrationcodesetId);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationCodeset findById(Integer hipclientlocationintegrationcodesetId) {
        LOGGER.debug("Finding HipClientLocationIntegrationCodeset by id: {}", hipclientlocationintegrationcodesetId);
        try {
            return this.wmGenericDao.findById(hipclientlocationintegrationcodesetId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No HipClientLocationIntegrationCodeset found with id: {}", hipclientlocationintegrationcodesetId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public List<HipClientLocationIntegrationCodeset> findByMultipleIds(List<Integer> hipclientlocationintegrationcodesetIds, boolean orderedReturn) {
        LOGGER.debug("Finding HipClientLocationIntegrationCodesets by ids: {}", hipclientlocationintegrationcodesetIds);

        return this.wmGenericDao.findByMultipleIds(hipclientlocationintegrationcodesetIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationCodeset update(HipClientLocationIntegrationCodeset hipClientLocationIntegrationCodeset) {
        LOGGER.debug("Updating HipClientLocationIntegrationCodeset with information: {}", hipClientLocationIntegrationCodeset);

        this.wmGenericDao.update(hipClientLocationIntegrationCodeset);
        this.wmGenericDao.refresh(hipClientLocationIntegrationCodeset);

        return hipClientLocationIntegrationCodeset;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationCodeset delete(Integer hipclientlocationintegrationcodesetId) {
        LOGGER.debug("Deleting HipClientLocationIntegrationCodeset with id: {}", hipclientlocationintegrationcodesetId);
        HipClientLocationIntegrationCodeset deleted = this.wmGenericDao.findById(hipclientlocationintegrationcodesetId);
        if (deleted == null) {
            LOGGER.debug("No HipClientLocationIntegrationCodeset found with id: {}", hipclientlocationintegrationcodesetId);
            throw new EntityNotFoundException(String.valueOf(hipclientlocationintegrationcodesetId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public void delete(HipClientLocationIntegrationCodeset hipClientLocationIntegrationCodeset) {
        LOGGER.debug("Deleting HipClientLocationIntegrationCodeset with {}", hipClientLocationIntegrationCodeset);
        this.wmGenericDao.delete(hipClientLocationIntegrationCodeset);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocationIntegrationCodeset> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all HipClientLocationIntegrationCodesets");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocationIntegrationCodeset> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all HipClientLocationIntegrationCodesets");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service HIP for table HipClientLocationIntegrationCodeset to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service HIP for table HipClientLocationIntegrationCodeset to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocationIntegrationApptTypeCharges> findAssociatedHipClientLocationIntegrationApptTypeChargeses(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated hipClientLocationIntegrationApptTypeChargeses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("hipClientLocationIntegrationCodeset.id = '" + id + "'");

        return hipClientLocationIntegrationApptTypeChargesService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service HipClientLocationIntegrationApptTypeChargesService instance
     */
    protected void setHipClientLocationIntegrationApptTypeChargesService(HipClientLocationIntegrationApptTypeChargesService service) {
        this.hipClientLocationIntegrationApptTypeChargesService = service;
    }

}