/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.healthipass.svcs.hip.HipClientLocationIntegrationProvider;


/**
 * ServiceImpl object for domain model class HipClientLocationIntegrationProvider.
 *
 * @see HipClientLocationIntegrationProvider
 */
@Service("HIP.HipClientLocationIntegrationProviderService")
@Validated
public class HipClientLocationIntegrationProviderServiceImpl implements HipClientLocationIntegrationProviderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HipClientLocationIntegrationProviderServiceImpl.class);


    @Autowired
    @Qualifier("HIP.HipClientLocationIntegrationProviderDao")
    private WMGenericDao<HipClientLocationIntegrationProvider, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<HipClientLocationIntegrationProvider, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationProvider create(HipClientLocationIntegrationProvider hipClientLocationIntegrationProvider) {
        LOGGER.debug("Creating a new HipClientLocationIntegrationProvider with information: {}", hipClientLocationIntegrationProvider);

        HipClientLocationIntegrationProvider hipClientLocationIntegrationProviderCreated = this.wmGenericDao.create(hipClientLocationIntegrationProvider);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(hipClientLocationIntegrationProviderCreated);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationProvider getById(Integer hipclientlocationintegrationproviderId) {
        LOGGER.debug("Finding HipClientLocationIntegrationProvider by id: {}", hipclientlocationintegrationproviderId);
        return this.wmGenericDao.findById(hipclientlocationintegrationproviderId);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationProvider findById(Integer hipclientlocationintegrationproviderId) {
        LOGGER.debug("Finding HipClientLocationIntegrationProvider by id: {}", hipclientlocationintegrationproviderId);
        try {
            return this.wmGenericDao.findById(hipclientlocationintegrationproviderId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No HipClientLocationIntegrationProvider found with id: {}", hipclientlocationintegrationproviderId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public List<HipClientLocationIntegrationProvider> findByMultipleIds(List<Integer> hipclientlocationintegrationproviderIds, boolean orderedReturn) {
        LOGGER.debug("Finding HipClientLocationIntegrationProviders by ids: {}", hipclientlocationintegrationproviderIds);

        return this.wmGenericDao.findByMultipleIds(hipclientlocationintegrationproviderIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationProvider update(HipClientLocationIntegrationProvider hipClientLocationIntegrationProvider) {
        LOGGER.debug("Updating HipClientLocationIntegrationProvider with information: {}", hipClientLocationIntegrationProvider);

        this.wmGenericDao.update(hipClientLocationIntegrationProvider);
        this.wmGenericDao.refresh(hipClientLocationIntegrationProvider);

        return hipClientLocationIntegrationProvider;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public HipClientLocationIntegrationProvider delete(Integer hipclientlocationintegrationproviderId) {
        LOGGER.debug("Deleting HipClientLocationIntegrationProvider with id: {}", hipclientlocationintegrationproviderId);
        HipClientLocationIntegrationProvider deleted = this.wmGenericDao.findById(hipclientlocationintegrationproviderId);
        if (deleted == null) {
            LOGGER.debug("No HipClientLocationIntegrationProvider found with id: {}", hipclientlocationintegrationproviderId);
            throw new EntityNotFoundException(String.valueOf(hipclientlocationintegrationproviderId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "HIPTransactionManager")
    @Override
    public void delete(HipClientLocationIntegrationProvider hipClientLocationIntegrationProvider) {
        LOGGER.debug("Deleting HipClientLocationIntegrationProvider with {}", hipClientLocationIntegrationProvider);
        this.wmGenericDao.delete(hipClientLocationIntegrationProvider);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocationIntegrationProvider> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all HipClientLocationIntegrationProviders");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<HipClientLocationIntegrationProvider> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all HipClientLocationIntegrationProviders");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service HIP for table HipClientLocationIntegrationProvider to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service HIP for table HipClientLocationIntegrationProvider to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "HIPTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}