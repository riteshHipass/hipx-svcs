/*Copyright (c) 2018-2019 healthipass.com All Rights Reserved.
 This software is the confidential and proprietary information of healthipass.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with healthipass.com*/
package com.healthipass.svcs.hip;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * HipClientLocationConfig generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`HipClientLocationConfig`")
public class HipClientLocationConfig implements Serializable {

    private Integer id;
    private int configId;
    private int clientLocationId;
    private String value;
    private String status = "disabled";
    private String description;
    private Integer createdBy;
    private Timestamp createdDateTime;
    private Integer updatedBy;
    private Timestamp updatedDateTime;
    private HipClientLocation hipClientLocation;
    private HipConfig hipConfig;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`configId`", nullable = false, scale = 0, precision = 10)
    public int getConfigId() {
        return this.configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    @Column(name = "`clientLocationId`", nullable = false, scale = 0, precision = 10)
    public int getClientLocationId() {
        return this.clientLocationId;
    }

    public void setClientLocationId(int clientLocationId) {
        this.clientLocationId = clientLocationId;
    }

    @Column(name = "`value`", nullable = false, length = 25)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "`status`", nullable = false, length = 8)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "`description`", nullable = false, length = 250)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "`createdBy`", nullable = true, scale = 0, precision = 10)
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "`createdDateTime`", nullable = false)
    public Timestamp getCreatedDateTime() {
        return this.createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Column(name = "`updatedBy`", nullable = true, scale = 0, precision = 10)
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "`updatedDateTime`", nullable = true)
    public Timestamp getUpdatedDateTime() {
        return this.updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`clientLocationId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkClientLocationConfigClientLocationId`"))
    @Fetch(FetchMode.JOIN)
    public HipClientLocation getHipClientLocation() {
        return this.hipClientLocation;
    }

    public void setHipClientLocation(HipClientLocation hipClientLocation) {
        if(hipClientLocation != null) {
            this.clientLocationId = hipClientLocation.getId();
        }

        this.hipClientLocation = hipClientLocation;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`configId`", referencedColumnName = "`id`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`fkClientLocationConfigConfigId`"))
    @Fetch(FetchMode.JOIN)
    public HipConfig getHipConfig() {
        return this.hipConfig;
    }

    public void setHipConfig(HipConfig hipConfig) {
        if(hipConfig != null) {
            this.configId = hipConfig.getId();
        }

        this.hipConfig = hipConfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HipClientLocationConfig)) return false;
        final HipClientLocationConfig hipClientLocationConfig = (HipClientLocationConfig) o;
        return Objects.equals(getId(), hipClientLocationConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}